/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/prop-types */
import React, { useState, useContext } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import useStyles from '../Styles/useStyles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import BASE_URL from '../../../common/constants';
import swal from 'sweetalert';
import AuthContext from '../../../providers/AuthContext';
import Loader from '../../Loader/Loader';

const UserDetails = ({ id, history, username, ban_status, roles_id, is_deleted, setAllUsers }) => {

  const { token } = useContext(AuthContext);

  const classes = useStyles();

  const [banStatus, setBanStatus] = useState(ban_status);
  const [loading, setLoading] = useState(false);

  const handleBanStatus = () => {
    setLoading(true);

    const newBanStatus = banStatus === 0 ? ({"banStatus": 1}) : ({"banStatus": 0});

    fetch(`${BASE_URL}/admin/users/${id}/banstatus`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify (newBanStatus),
    })
    .then(response => {
      if (response.status >= 400) {
        throw new Error ('Bad Request');
      };
      return response.json()
    })
    .then(data => {
      swal(data.message, "", "success")
      setBanStatus(data.user.ban_status);
    })
    .catch((error) => swal("The users are not found.", "", "error"))
    .finally(() => setLoading(false));
  };

  const handleDelete = () => {

    swal({
      title: 'Are you sure you want to delete this user?',
      text: "This action can not be undone",
      // icon: "warning",
      buttons: ['No', 'Yes'],
      dangerMode: false,
    })
    .then((willDelete) => {
      if (willDelete) {

        setLoading(true);

        fetch(`${BASE_URL}/admin/users/${id}`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          if (response.status >= 400) {
            throw new Error('Bad Request')
          }
          response.json()
        })
        .then((data) => setAllUsers((users) => users.filter((u) => u.id !== id)))
        .catch((err) => swal("The user cannot be deleted. He is probably a reader.", "", "error"))
        .finally(() => setLoading(false));
      } else {
        // swal("You can continue to read books!");
        return null
      }
    });

    
  };

  // const imgStyles = {
  //     card: {
  //         maxWidth: 345,
  //     },
  //     media: {
  //         height: "28em",
  //         paddingTop: '50%', // 16:9
  //     },
  // };

  if (loading) {
    return <Loader />
  }

  return (
    <Grid item key={id} xs={12} sm={6} md={4}>
      <Card className={classes.user}>
        {/* <CardMedia
          className={classes.cardMedia}
          style={imgStyles.media}
          image={url}
          title="Image title"
        /> */}
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            User ID: {id}
          </Typography>
          <Typography>
            Username: {username}
          </Typography>
          {(roles_id !== 2) ? <>
            {(banStatus === 0) ?
              <Typography color="primary">
                Ban status: NOT BANNED
              </Typography> :
              <Typography color="secondary">
                Ban status: BANNED
              </Typography>}
            {roles_id === 1 ?
              <Typography color="primary">
                Role: User
              </Typography> :
              <Typography color="secondary">
                Admin
              </Typography>}
            {is_deleted === 1 ?
              <Typography color="primary">
                Deleted
              </Typography> : <></>} </> :
            null}
        </CardContent>
        <CardActions>
          {/* <Button size="small" color="primary">
            Edit
          </Button> */}
          {roles_id !== 2 ? <>
            {banStatus === 1 ?
              <Button size="small" color="primary" onClick={handleBanStatus}>
                UNBAN
              </Button> :
              <Button size="small" color="primary" onClick={handleBanStatus}>
                BAN
              </Button>}
            <Button size="small" color="primary" onClick={(e) => { handleDelete(); e.preventDefault() }}>
              DELETE
            </Button> </> :
          null}
        </CardActions>
      </Card>
    </Grid>
  )
};

UserDetails.propTypes = {
  id: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  username: PropTypes.string.isRequired,
  ban_status: PropTypes.number.isRequired,
  roles_id: PropTypes.number.isRequired,
  is_deleted: PropTypes.number.isRequired,
}
export default withRouter(UserDetails);
