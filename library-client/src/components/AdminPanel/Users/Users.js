import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import BASE_URL from '../../../common/constants';
import swal from 'sweetalert';
import Loader from '../../Loader/Loader';
import UserDetails from './UserDetails';
import { Container, Grid, Typography } from '@material-ui/core';
import AuthContext from '../../../providers/AuthContext';
import useStyles from '../Styles/useStyles';
import Button from '@material-ui/core/Button';

const Users = ({ history }) => {
  const { token } = useContext(AuthContext);

  const classes = useStyles();

  const [allUsers, setAllUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleHomeButton = () => history.push('/admin/home')

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/admin/users`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => response.json())
      .then(data => {
        setAllUsers(data)
      })
      .catch((error) => swal("The users are not found.", "", "error"))
      .finally(() => setLoading(false));
  }, [token]);

  if (loading) {
    return <Loader />
  }

  const transformedUsers = allUsers.map((user) => {

    if (user.is_deleted === 0) {
      return (
        <UserDetails key={user.id} {...user} allUsers={allUsers} setAllUsers={setAllUsers} />
      )
    }
    return null;

  })
  return (
    <div className={classes.heroContent}>
      <Container maxWidth="sm">
        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
          User's Info Board
        </Typography>
        <div className={classes.heroButtons}>
          <Grid container spacing={2} justify="center">
            <Grid item>
              <Button variant="contained" color="primary" onClick={handleHomeButton}>
                Go to Admin Home Page
              </Button>
            </Grid>
          </Grid>
        </div>
      </Container>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          {transformedUsers}
        </Grid>
      </Container>
    </div>)
}

Users.propTypes = {
  history: PropTypes.object.isRequired,
}
export default Users;

