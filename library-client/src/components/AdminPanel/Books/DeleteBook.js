import swal from 'sweetalert';
import BASE_URL from '../../../common/constants';

const DeleteBook = (id, token, history) => {

  fetch(`${BASE_URL}/admin/books/${id}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    mode: "cors",
  })
  .then((response) => {
    if (response.status >= 400) {
      return (swal("The books is not found", "", "error"))
    }
    return response.json();
  })
  .then((data) => {
    (swal(data.message, "", "success"));
    // eslint-disable-next-line react/prop-types
    history.push('/books')
  })
  .catch((error) => swal(error.message, "", "error"))
  // .finally(() => setLoading(false));
}

export default DeleteBook;
