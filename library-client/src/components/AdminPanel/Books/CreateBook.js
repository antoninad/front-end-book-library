import swal from 'sweetalert';
import Swal from 'sweetalert2';
import BASE_URL from '../../../common/constants';

const userErrorAlert = () => { return (swal("Oops...  404 Bad Request", "", "error")) };
const rangeErrorAlert = () => { return (swal("The content should be in range [3...25].", "", "error")) };

const CreateBook = async (token, bookAdded, setBookAdded) => {

    const { value: formValues } = await Swal.fire({
        title: 'Create a book here...',
        html:
            '<input placeholder="Title" id="swal-input1" class="swal2-input">' +
            '<input placeholder="Author" id="swal-input2" class="swal2-input">'+
            '<input placeholder="Book cover URL" id="swal-input3" class="swal2-input">',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            return [
                document.getElementById('swal-input1').value,
                document.getElementById('swal-input2').value,
                document.getElementById('swal-input3').value
            ]
        }
    })

    if (formValues) {
        // const params = Swal.fire(JSON.stringify(formValues))
        const bookTitle = formValues[0];
        const bookAuthor = formValues[1];
        const bookCover = formValues[2];

        fetch(`${BASE_URL}/admin/books`, {
            method: "POST",
            body: JSON.stringify({ 
                title: bookTitle, 
                author: bookAuthor, 
                url: bookCover 
            }),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
        .then((response) => {
            if (response.status >= 400) {
                userErrorAlert();
            }
            
            response.json()
        })
        .then((data) => {
            if (!formValues[0] || !formValues[1] ||
                formValues[0].length < 3 || formValues[0].length > 25 ||
                formValues[1].length < 3 || formValues[1].length > 25) {
                rangeErrorAlert();
            }
            
        })
        .finally(() => {
            setBookAdded(bookAdded+1);
        })
    }
}

export default CreateBook;
