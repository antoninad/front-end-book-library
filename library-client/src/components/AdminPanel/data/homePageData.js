const homePageData = [
    {name: 'Books', 
     text: 'As ADMIN you can view all books, read books, create new books and delete books.',
     path: '/books',
     url: 'https://www.rd.com/wp-content/uploads/2017/10/This-Is-How-Long-It-Takes-To-Read-The-Whole-Dictionary_509582812-Billion-Photos_FB-e1574101045824.jpg'
    }, 
    {name: 'Reviews', 
     text: 'As ADMIN you can see report with reviews for each book, update and delete existing reviews.', 
     path: '/admin/reviews',
     url: 'https://www.reviewme.com/wp-content/uploads/2019/07/reviews.png'
    }, 
    {name: 'Users', 
     text: 'As ADMIN you can ban/unban and delete users, but only if they are not currently reading a book.',
     path: '/admin/users',
     url: 'https://tadabase.io/staticassets/images/illustrations/users-top.png'
    }
];

export default homePageData; 
