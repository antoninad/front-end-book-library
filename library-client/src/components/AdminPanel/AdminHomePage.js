/* eslint-disable react/jsx-handler-names */
/* eslint-disable react/prop-types */
import React from 'react';
// import AuthContext from '../../providers/AuthContext';
import Button from '@material-ui/core/Button';
import useStyles from './Styles/useStyles';
// import { Container, Typography } from '@material-ui/core';
// import AppBar from '@material-ui/core/AppBar';
// import Button from '@material-ui/core/Button';
// import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
// import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
// import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
// import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import homePageData from './data/homePageData';
// import Link from '@material-ui/core/Link';

const AdminHomePage = (props) => {
    // const { token } = useContext(AuthContext);
  
    const classes = useStyles();
    // const handleUsersButton = () => props.history.push(`/admin/users/`);
   

    return (
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
            Admin Home Page
          </Typography>
        </Container>     
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {homePageData.map((el) => (
              <Grid item key={homePageData.indexOf(el)} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={el.url}
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {el.name}
                    </Typography>
                    <Typography>
                      {el.text}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" color="primary" onClick={() => props.history.push(`${el.path}`)}>
                      View
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </div>
    )

}

export default AdminHomePage;
