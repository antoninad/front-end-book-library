import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import BASE_URL from '../../../common/constants';
import swal from 'sweetalert';
import Loader from '../../Loader/Loader';
import AdminReviewDetails from './AdminReviewDetails';
import { Container, Grid, Typography } from '@material-ui/core';
import AuthContext from '../../../providers/AuthContext';
import useStyles from '../Styles/useStyles';
import Button from '@material-ui/core/Button';
import RevContext from './RevContext';

const AdminReviews = ({ history }) => {
  const { token } = useContext(AuthContext);

  const classes = useStyles();
  const { reviewsData, setReviewsState } = useContext(RevContext);
  const [loading, setLoading] = useState(false);

  const handleHomeButton = () => history.push('/admin/home')

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/admin/reviews`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => {
          if (response.status >= 400) {
            throw new Error ('Bad Request');
          }
          return response.json()
        })
      .then(data => {
     
        setReviewsState({
          reviewsData: data,
        });
      })
      .catch((error) => swal("Reviews are not found.", "", "error"))
      .finally(() => setLoading(false));
  }, [token]);

  if (loading) {
    return <Loader />
  }


  const transformedReviews = reviewsData.map((review) => {
    if (review.isDeleted === 0) {
      return (
        <AdminReviewDetails key={review.reviewId} {...review} />
      )
    }
    return null;
  })

  return (
    <div className={classes.heroContent}>
      <Container maxWidth="sm">
        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
          Reviews Info
        </Typography>
        <div className={classes.heroButtons}>
          <Grid container spacing={2} justify="center">
            <Grid item>
              <Button variant="contained" color="primary" onClick={handleHomeButton}>
                Go to Admin Home Page
              </Button>
            </Grid>
          </Grid>
        </div>
      </Container>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          {transformedReviews}
        </Grid>
      </Container>
    </div>
    )
}

AdminReviews.propTypes = {
  history: PropTypes.object.isRequired,
}
export default AdminReviews;

