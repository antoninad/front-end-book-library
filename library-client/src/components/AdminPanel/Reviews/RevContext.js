import { createContext } from 'react';

const RevContext = createContext({
  reviewsData: [],
  update: false,
  setReviewsState: () => {},
});

export default RevContext;
