import BASE_URL from '../../../common/constants';

import swal from 'sweetalert';

const DeleteReviewAdmin = async (token, bookId, reviewId, reviewsData, setReviewsState) => {
    const userErrorAlert = () => { return (swal("Oops... 404 Bad Request", "", "error")) };
    swal({
        title: 'Are you sure you want to delete this review?',
        text: "This action can not be undone",
        // icon: "warning",
        buttons: ['No', 'Yes'],
        dangerMode: false,
    })
    .then((willDelete) => {
        if (willDelete) {
            fetch(`${BASE_URL}/books/${bookId}/reviews/${reviewId}`, {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
            .then((response) => {
                if (response.status >= 400) {
                    throw new Error();
                }
                return response.json()
            })
            .then((data) => {
                const updatedRevs = reviewsData.filter((review) => review.reviewId !== reviewId);
                setReviewsState({
                    reviewsData: updatedRevs
                });
            })
            .catch((err) => userErrorAlert);
            
        } else {
            return null;
        }
    })

}

export default DeleteReviewAdmin;
