import React, { useContext, useState } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import useStyles from '../Styles/useStyles';
import Grid from '@material-ui/core/Grid';
// import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditOutlined from '@material-ui/icons/EditOutlined';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import AdminUpdateReview from './AdminUpdateReview';
import RevContext from './RevContext';
import AuthContext from '../../../providers/AuthContext';
import DeleteReviewAdmin from './DeleteReviewAdmin'
// create allReviews CONTEXT
const AdminReviewDetails = ({ bookId, content, reviewId, title, userId, username }) => {

  const { token } = useContext(AuthContext);
  const { reviewsData, setReviewsState } = useContext(RevContext);
  const classes = useStyles();

  const [deletedRevId, setDeletedRevIdv] = useState(0);


  return (
    <Grid item key={reviewId} xs={12} sm={12} md={10} display="block">
      <Card className={classes.id}>
        {/* <CardMedia
          className={classes.cardMedia}
          style={imgStyles.media}
          image='https://media.wired.com/photos/5be4cd03db23f3775e466767/master/w_2560%2Cc_limit/books-521812297.jpg'
          title="Image title"
        /> */}
        <CardContent className={classes.cardContent}>
          <Box fontWeight="fontWeightBold" fontSize="h6.fontSize">
            {content}
          </Box>
          <Typography variant="body1" color="textSecondary" component="h2">
            @{username}
          </Typography>
          <Typography gutterBottom variant="body2" color="textSecondary">
            user id: {userId}
          </Typography>
          <Typography component="h2">
            Title: {title}
          </Typography>
          <Typography gutterBottom variant="body2">
            book id: {bookId}
          </Typography>
        </CardContent>
        <CardActions>
          <IconButton aria-label="delete" onClick={(e) => { e.preventDefault(); DeleteReviewAdmin(token, bookId, reviewId, reviewsData, setReviewsState);}}>
            <DeleteIcon />
          </IconButton>
          <IconButton aria-label="edit" onClick={(e) => { e.preventDefault(); AdminUpdateReview(token, bookId, reviewId, reviewsData, setReviewsState) }}>
            <EditOutlined />
          </IconButton>
          {/* <Button size="small" color="primary">
            DELETE
          </Button> */}
        </CardActions>
      </Card>
    </Grid>
  )
};

AdminReviewDetails.propTypes = {
  bookId: PropTypes.number.isRequired,
  content: PropTypes.string.isRequired,
  reviewId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  // setAllReviews: PropTypes.func.isRequired,
}
export default withRouter(AdminReviewDetails);
