import swal from 'sweetalert';
import Swal from 'sweetalert2';
import BASE_URL from '../../../common/constants';

const AdminUpdateReview = async (token, bookId, reviewId, reviewsData, setReviewsState) => {
    
    const userErrorAlert = () => { return (swal("Oops... 404 Bad Request", "", "error")) };
    const rangeErrorAlert = () => { return (swal("The content should be in range [3...25].", "", "error")) };

    const { value: text } = await Swal.fire({
        input: 'textarea',
        inputLabel: 'Your updated review',
        inputPlaceholder: 'Update your review here...',
        inputAttributes: {
            'aria-label': 'Update your review here...'
        },
        showCancelButton: true
    });

    if (text) {

        if (!text || text.length < 3 || text.length > 25) {
            return rangeErrorAlert();
        }

        fetch(`${BASE_URL}/admin/${bookId}/reviews/${reviewId}`, {
            method: "PUT",
            body: JSON.stringify({ content: text }),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
        .then((response) => {
            if (response.status >= 400) {
                throw new Error ();
            }
            return response.json()
        })
        .then((data) => {
        
            const updatedRevs = reviewsData.map((rev) => {
                if (rev.reviewId === reviewId) {
                    rev.content = text;
                }
                return rev
            });

            setReviewsState({
                reviewsData: updatedRevs
            });
        })
        .catch((error) => {
            return userErrorAlert
        })
    }
}

export default AdminUpdateReview;
