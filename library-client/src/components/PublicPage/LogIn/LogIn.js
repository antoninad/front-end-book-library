import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from '../Styles/useStyle';
import Loader from '../../Loader/Loader';
import swal from 'sweetalert';
// import Swal from 'sweetalert2';
import AuthContext, { extractUser } from '../../../providers/AuthContext';
import BASE_URL from '../../../common/constants';
import Toast from './SuccessMessage';
// import { withRouter } from 'react-router-dom';

const SignIn = ({ history, location, match }) => {

  const classes = useStyles();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  // const login = location.pathname === '/login';

  const { isLoggedIn, setLoginState, role } = useContext(AuthContext);
  
  useEffect(() => {
    if (isLoggedIn) {
      if (role === 'User') {
        history.push('/books');
      }

      if (role === 'Admin') {
        history.push('/admin/home');
      }
    }
  }, [history, isLoggedIn, role]);

  const handleUsername = event => {
    if (!event) {
      throw Error
    }
    setUsername(event.target.value)
  };

  const handlePassword = event => {
    if (!event) {
      throw Error
    }
    setPassword(event.target.value)
  }

  const handleSubmit = () => {
    setLoading(true);

    fetch(`${BASE_URL}/auth/signin`, { 
      method: "POST",
      body: JSON.stringify({
        username,
        password
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((response) => response.json())
    .then(({ token }) => {
      if(!token) {
        throw Error;
      }

      localStorage.setItem('token', token);

      setLoginState({
        isLoggedIn: !!extractUser(token),
        user: extractUser(token),
        token: token,
        role: extractUser(token).role,
        banStatus: extractUser(token).banStatus
      });
      
      Toast.fire({
        icon: 'success',
        title: 'Signed in successfully'
      })
    })
    .catch((error) => swal("Wrong username or password!", "", "error"))
    .finally(() => setLoading(false));
  }
  
  if (loading) {
    return <Loader />
  }

  const clickRegister = () => {
    // eslint-disable-next-line react/prop-types
    history.push('/signup');
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            onChange={handleUsername}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={handlePassword}
            
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
          >
            Log In
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" variant="body2" onClick={clickRegister}>
                Don't have an account? Create now
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

SignIn.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default SignIn;
