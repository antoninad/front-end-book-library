import React, {useState}from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import swal from 'sweetalert';
import useStyles from '../Styles/useStyle';
import Loader from '../../Loader/Loader';
import BASE_URL from '../../../common/constants';

const Register = ({ history }) => {
  const classes = useStyles();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordCheck, setPasswordCheck] = useState('');
  const [loading, setLoading] = useState(false);

  const handleUsername = event => {
    if (!event) {
      throw Error
    }
    setUsername(event.target.value)
  };

  const handlePassword = event => {
    if (!event) {
      throw Error
    }
    setPassword(event.target.value)
  }

  const handlePasswordCheck = event => {
    if (!event) {
      throw Error
    }
    setPasswordCheck(event.target.value)
  }

  if (loading) {
    return <Loader />
  }

  const handleRegister = () => {
    if(!username || !password || !passwordCheck){
      return swal("Wrong username or password!", "", "error")
    }

    if(password.length < 3 || password.length > 25 || username.length < 3 || username.length > 25){
      return swal("Wrong username or password!", "", "error")
    }

    if(password !== passwordCheck){
     return swal("Password inputs are not matching!", "", "error")
      // throw Error
    }
    setLoading(true);

      fetch(`${BASE_URL}/users`, { 
      method: "POST",
      body: JSON.stringify({
        username,
        password
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((res) => res.json())
    .then((data) => {
      if(data.message){
        // alert("wrong")
        throw Error
      }
      setPasswordCheck('')
      setPassword('')
      setUsername('')
      swal("Your account was created successfully! Please, login!", "", "success")
      history.push('/signin');
    })
    .catch((error) => swal("User name is existing. Try login.", "", "error"))
    .finally(() => setLoading(false));
  }

  const clickLogin = () => {
    history.push('/signin');
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                onChange={handleUsername}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handlePassword}

              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handlePasswordCheck}
              />
            </Grid>
            {/* <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid> */}
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleRegister}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" variant="body2" onClick={clickLogin}>
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      {/* <Box mt={5}>
        <Copyright />
      </Box> */}
    </Container>
  );
}

Register.propTypes = {
  history: PropTypes.object.isRequired,
}

export default Register;
