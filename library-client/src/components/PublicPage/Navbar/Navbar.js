import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';
import { Button } from './Button/Button';
import { withRouter } from 'react-router-dom'; 
import './Navbar.css';
import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import swal from 'sweetalert';

const Navbar = ({ history }) => {

  const [click, setClick] = useState(false);
  const {isLoggedIn, setLoginState, user} = useContext(AuthContext);

  const closeMobileMenu = () => setClick(false);

  const handleLoginButton = () => {
    history.push('/signin');
  };

  const handleLogoutButton = () => {
    swal({
      title: "Do you really want to logout?",
      // icon: "warning",
      buttons: ['No', 'Yes'],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Poof! You just logged out!", {
          icon: "success",
        });
        setLoginState(false);
        localStorage.removeItem('token');
      } else {
        // swal("You can continue to read books!");
        return null
      }
    });

    // setLoginState(false);
    // localStorage.removeItem('token');
    
  };

  return (
    <>
      <nav className="navbar">
        <div className="navbar-container">
          <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
            LIBRARY <i className="fa fa-book" aria-hidden="true" />
          </Link>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            {(user) ?
              (
                <Grid container justify="flex-end">
                  <Typography id="greeting" variant='h5' color='secondary'>
                    <div style={{color: 'white'}}>
                      {`Hello, ${user.username}!`}
                    </div>
                  </Typography>
                </Grid>
              ) :
            null}
            {(isLoggedIn) ? 
            (<Button buttonStyle='btn--outline' onClick={handleLogoutButton}>LOG OUT</Button>) :
            (<Button buttonStyle='btn--outline' onClick={handleLoginButton}>LOG IN</Button>)}
          </ul>
        </div>
      </nav>
    </>
  )
}

Navbar.propTypes = {
  history: PropTypes.object.isRequired,
}

export default withRouter(Navbar);
