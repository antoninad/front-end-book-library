import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import '../../../../App.css';
import { Button } from '../../Navbar/Button/Button';
import './HeroSection.css';
import HeroImage from './HeroImage/heroSection.jpg';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../../../providers/AuthContext';

const HeroSection = ({ history }) => {

  const { user, isLoggedIn } = useContext(AuthContext);
  

  const handleRegisterButton = () => {
    history.push('/signup')
  };

  const handeAllBooksButton = () => {
    history.push('/books')
  };

  const handeAdminHomeButton = () => {
    history.push('/admin/home');
  }

  return (
    <div className='hero-container'>
      <img src={HeroImage} alt='HeroImage' />
      <h1>WELCOME TO OUR LIBRARY</h1>
      <p>Keep calm and read a book.</p>
      <div className='hero-btns'>
        {(isLoggedIn && user.role === 'Admin') ?
          <Button
            className='btns'
            to="/signup"
            buttonStyle='btn--outline'
            buttonSize='btn--large'
            onClick={handeAdminHomeButton}
          >
            Admin Home Page
          </Button> :
            null}
        {(isLoggedIn) ?
          <Button
            className='btns'
            to="/signup"
            buttonStyle='btn--outline'
            buttonSize='btn--large'
            onClick={handeAllBooksButton}
          >
            Go to all books
          </Button> :
          <Button
            className='btns'
            to="/signup"
            buttonStyle='btn--outline'
            buttonSize='btn--large'
            onClick={handleRegisterButton}
          >
            Create your account
          </Button>}
      </div>
    </div>
  )
}

HeroSection.propTypes = {
  history: PropTypes.object.isRequired,
}

export default withRouter(HeroSection);
