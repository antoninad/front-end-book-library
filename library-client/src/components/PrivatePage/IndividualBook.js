import React, { useEffect, useState, useContext } from 'react';
import BookDetails from './BookDetails';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import useStyles from './Syles/useStyles';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Loader from '../Loader/Loader';
import Alert from '@material-ui/lab/Alert';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import AllReviews from './Reviews/AllReviews';
import swal from 'sweetalert';
// import Swal from 'sweetalert2';
import BASE_URL from '../../common/constants';
import CreateReview from './Reviews/CreateReview';
import CreateRating from './Ratings/CreateRating';
import AuthContext from '../../providers/AuthContext';
import DeleteBook from '../AdminPanel/Books/DeleteBook';

// eslint-disable-next-line react/prop-types
const IndividualBook = ({ history, match }) => {

  const { user, token } = useContext(AuthContext);
  const userRole = user.role;

  const classes = useStyles();

  const [bookData, setBookData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(null);
  const [viewReviews, toggleViewReviews] = useState(false);

  // eslint-disable-next-line react/prop-types
  const { id } = match.params;
  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/books/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => {
        if (response.status !== 200) {
          throw new Error('Resource not found!');
        }
        return response.json();
      })
      .then(data => {
        return setBookData(data);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id, status, token]);

  const borrowBookAlert = () => { return (swal("You successfully borrowed the book!", "", "success")) };
  const borrowBookErrorAlert = () => { return (swal("This book is already borrowed. Please, choose another one.", "", "error")) };

  const handleBorrowButton = () => {

    setLoading(true);

    fetch(`${BASE_URL}/books/${id}`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      mode: "cors",
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.borrowedBook) {
          borrowBookAlert()
        }
        return setStatus(data.borrowedBook.status);
      })
      .catch((error) => borrowBookErrorAlert())
      .finally(() => setLoading(false));
  }

  const returnBookAlert = () => { return (swal("You successfully returned the book!", "", "success")) };
  const returnBookErrorAlert = () => { return (swal("You can not return this book", "", "error")) };

  const handleReturnButton = () => {

    setLoading(true);

    fetch(`${BASE_URL}/books/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      mode: "cors",
    })
      .then((response) => {
        if (response.status >= 400) {
          throw new Error('Bad Request');
        }

        return response.json();
      })
      .then((data) => {
        returnBookAlert();
        setStatus(data.returnedBook.status);
      })
      .catch((error) => returnBookErrorAlert())
      .finally(() => setLoading(false));
  }

  const handleReviewsButton = () => toggleViewReviews(!viewReviews);

  const handleDeleteBookADMIN = () => {

    swal({
      title: 'Are you sure you want to delete this book?',
      text: "This action can not be undone",
      // icon: "warning",
      buttons: ['No', 'Yes'],
      dangerMode: false,
    })
    .then((willDelete) => {
      if (willDelete) {
        // book deleting 
        DeleteBook(id, token, history);
        
      } else {
        return null
      }
    });
  };

  const showReviews = () => {
    if (viewReviews) {
      return <AllReviews id={+id} />
    }
  }

  if (error) {
    return (
      <Grid item xs={12} sm={6} md={4}>
        <Alert severity="error">{error}</Alert>
        <nav>
          <NavLink to="/books">All books</NavLink>
        </nav>
      </Grid>
    )
  }

  if (loading) {
    return <Loader />
  }

  return (bookData && (
    <div>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          <BookDetails {...bookData} isIndividual />
          <Container maxWidth="sm">
            <h2>What is Lorem Ipsum?</h2>
            <Typography variant="body1" display="block" gutterBottom>
              Lorem Ipsum is simply dummy text of the printing and
              typesetting industry. Lorem Ipsum has been the industry's
              standard dummy text ever since the 1500s, when an unknown
              printer took a galley of type and scrambled it to make
              a type specimen book. It has survived not only five centuries,
              but also the leap into electronic typesetting, remaining
              essentially unchanged. It was popularised in the 1960s with
              the release of Letraset sheets containing Lorem Ipsum passages,
              and more recently with desktop publishing software like Aldus PageMaker
              including versions or Lorem Ipsum.
            </Typography>
            {/* Admin-a може ли да borrow-ва и return-ва??? */}
            <br />
            {user.banStatus === 0 ?
              <>
                <p>Rate the book here</p>
                <CreateRating id={id} /> 
              </> : 
            null}
            <br />
            <CardActions> 
              {(bookData.status === 'free') ?               
                <Button variant="outlined" color="primary" onClick={handleBorrowButton}>
                  Borrow
                </Button> :
              null}
              {(bookData.status === 'borrowed' && user.sub === bookData.curr_reader_id) ?
                <Button variant="outlined" color="primary" onClick={handleReturnButton}>
                  Return
                </Button> :
              null}
              {/* <CreateRating id={id} /> */}
              {user.role === 'Admin' ?
                <Button variant="outlined" color="primary" onClick={handleDeleteBookADMIN}>
                  Delete Book
                </Button> :
                null}
              {user.banStatus === 0 ?
                <>
                  <Button variant="outlined" onClick={handleReviewsButton}>
                    {viewReviews ? 'Hide Reviews' : 'Show Reviews'}
                  </Button> 
                  <Button variant="outlined" onClick={(e) => { CreateReview(id, token, toggleViewReviews); e.preventDefault() }}>
                    Create a review
                  </Button> 
                </> :
              null}
            </CardActions>
            {showReviews()}
          </Container>
        </Grid>
      </Container>
    </div>
  ))
};

IndividualBook.propTpes = {
  id: PropTypes.number.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

export default withRouter(IndividualBook);
