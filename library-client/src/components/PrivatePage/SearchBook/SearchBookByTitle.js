import React from 'react';
import SearchBar from 'material-ui-search-bar';
import PropTypes from 'prop-types';

const Search = (props) => {
  return (
    <SearchBar
      onKeyDown={props.handleChange}
      onRequestSearch={props.handleChange}
      type='text'
      placeholder='Search by title'
      style={{
        margin: '0 auto',
        maxWidth: 800,
        backgroundColor: 'none'
      }}
    />
  );

}

Search.propTypes = {
  handleChange: PropTypes.func.isRequired,
}

export default Search;
