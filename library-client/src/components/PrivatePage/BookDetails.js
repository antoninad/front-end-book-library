import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import useStyles from './Syles/useStyles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const BookDetails = ({id, history, url, author, title, isIndividual, status}) => {
  const classes = useStyles();
    
  const handleBackButton = () => history.goBack();
  const handleViewButton = () => history.push(`/books/${id}`);
  const renderViewButton = () => {
    if(!isIndividual) {
      return (
        <Button size="small" color="primary" onClick={handleViewButton}>
          View
        </Button>
      )
    }
  }

  const imgStyles = {
    card: {
      maxWidth: 345,
    },
    media: {
      height: "28em",
      paddingTop: '50%', // 16:9
    },
  };


  return (
    <Grid item key={id} xs={12} sm={6} md={4}>
      <Card className={classes.book}>
        <CardMedia
          className={classes.cardMedia}
          style={imgStyles.media}
          image={url}
          title="Image title"
        />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Typography>
            {author}
          </Typography>
          {status === 'free' ?
            <Box fontSize="body1.fontSize" color="success.main">
              available
            </Box> :
            <Box fontSize="body1.fontSize" color="error.main">
              {status}
            </Box>}
        </CardContent>
        <CardActions>
          {renderViewButton()}
          {/* <Button size="small" color="primary">
            Edit
          </Button> */}
          {isIndividual &&             
            <Button size="small" color="primary" onClick={handleBackButton}>
              Back
            </Button>}
        </CardActions>
      </Card>
    </Grid>
  )
}

BookDetails.propTypes = {
  id: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  url: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isIndividual: PropTypes.bool.isRequired,
  status: PropTypes.string.isRequired
}
export default withRouter(BookDetails);
