import swal from 'sweetalert';
import Swal from 'sweetalert2';
import BASE_URL from '../../../common/constants';
// import AuthContext from '../../../providers/AuthContext';
// import React, { useState, useContext } from 'react';
// import Loader from '../../Loader/Loader';


const CreateReview = async (id, token, toggleViewReviews) => {

    const readErrorAlert = () => { return (swal("You have to read the book first.", "", "error")) };
    const rangeErrorAlert = () => { return (swal("The content should be in range [3...25].", "", "error")) };

    const { value: text } = await Swal.fire({
        input: 'textarea',
        inputLabel: 'Your review',
        inputPlaceholder: 'Type your review here...',
        inputAttributes: {
            'aria-label': 'Type your review here...'
        },
        showCancelButton: true
    })
    if (text) {

        fetch(`${BASE_URL}/books/${id}/reviews`, {
            method: 'POST',
            body: JSON.stringify({ content: text }),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(data => {
                if (data.message) {
                    readErrorAlert();
                }
                if (text.length < 3 || text.length > 25 || !text) {
                    rangeErrorAlert();
                }
            })
            .finally(() => { toggleViewReviews(false); toggleViewReviews(true) })
    }
}

export default CreateReview;
