import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    card: {
      display: 'flex',
    },
    cardDetails: {
      flex: 5,
      marginBottom: 20,
      marginLeft: 20,
      marginTop: 20,
    },
    cardMedia: {
      width: 160,
      paddingTop: 20,
    },
    root: {
      '& > *': {
        marginLeft: 400,
      },
    },
});

export default useStyles;

