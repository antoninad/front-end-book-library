import BASE_URL from '../../../common/constants';
import swal from 'sweetalert';

const DeleteReview = (id, reviewId, token, reviewsData, setReviewsState,setDeletedRevId) => {
    const userErrorAlert = () => { return (swal("Oops... 404 Bad Request", "", "error")) };
    // setLoading(true);
    swal({
        title: 'Are you sure you want to delete this review?',
        text: "This action can not be undone",
        // icon: "warning",
        buttons: ['No', 'Yes'],
        dangerMode: false,
    })
    .then((willDelete) => {
        if (willDelete) {

          fetch(`${BASE_URL}/books/${id}/reviews/${reviewId}`, {
            method: "DELETE",
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((response) => {
              
            if (response.status >= 400) {
              throw new Error('Bad Request')
            }
            return response.json()
          })
          .then((data) => {
            const newData = reviewsData.filter((rev) => rev.id !== reviewId)
            setReviewsState(newData);
            setDeletedRevId(reviewId);
          })
          .catch((error) => {
            return userErrorAlert;
        })

        } else {
            return null;
        }
    })
    
}

export default DeleteReview;
