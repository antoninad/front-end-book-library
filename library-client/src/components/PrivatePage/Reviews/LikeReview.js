import BASE_URL from '../../../common/constants';
import swal from 'sweetalert';
// import React from 'react';

const LikeReview = (value, bookId, reviewId, token, setLikes, setDislikes, setLoading, changeVote, setChangeVote) => {

    setLoading(true);

    fetch(`${BASE_URL}/books/${bookId}/reviews/${reviewId}/votes`, {
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
        },
        method: "PUT", 
        body: JSON.stringify({"vote": value})
    })
    .then((response) => {
        if (response.status >= 400) {
            return (swal("You can't like/dislike reviews", "", "error"))
        }
        return response.json();
    })
    .then((data) => {
        setLikes(data.likes);
        setDislikes(data.dislikes);
        setChangeVote(!changeVote);
    })
    .catch((err) => swal(err.message, "", "error"))
    .finally(() => setLoading(false));    
}

export default LikeReview;
