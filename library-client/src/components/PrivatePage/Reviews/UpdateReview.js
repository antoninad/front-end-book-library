import swal from 'sweetalert';
import Swal from 'sweetalert2';
import BASE_URL from '../../../common/constants';

const UpdateReview = async (id, token, reviewId, reviewsData, setReviewsData, oldContent) => {

    const userErrorAlert = () => { return (swal("You have to be the author of the review to edit it.", "", "error")) };
    const rangeErrorAlert = () => { return (swal("The content should be in range [3...25].", "", "error")) };

    const { value: text } = await Swal.fire({
        input: 'textarea',
        inputLabel: 'Your updated review',
        inputPlaceholder: oldContent,
        inputAttributes: {
            'aria-label': 'Update your review here...'
        },
        showCancelButton: true
    });

    if (text) {

        fetch(`${BASE_URL}/books/${id}/reviews/${reviewId}`, {
            method: "PUT",
            body: JSON.stringify({ content: text }),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                if (response.status >= 400) {
                    userErrorAlert();
                }
                response.json()
            })
            .then((data) => {
                if (!text || text.length < 3 || text.length > 25) {
                    rangeErrorAlert();
                }
            })
            .finally(() => {
                const newReviewsData = reviewsData.map((review) => {
                    if (reviewId === review.reviewId) {
                        review.content = text;
                        return review;
                    }
                    else {
                        return review;
                    }
                })
                setReviewsData(newReviewsData);
            });
    }
};

export default UpdateReview;
