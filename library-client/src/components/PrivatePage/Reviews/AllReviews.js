import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Alert from '@material-ui/lab/Alert';
// import { NavLink } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import EditOutlined from '@material-ui/icons/EditOutlined';
import IconButton from '@material-ui/core/IconButton';
import ThumbDown from '@material-ui/icons/ThumbDown';
import ThumbUp from '@material-ui/icons/ThumbUp';
import BASE_URL from '../../../common/constants';
import Loader from '../../Loader/Loader';
import useStyles from './Style/ReviewStyle';
import AuthContext from '../../../providers/AuthContext';
import UpdateReview from './UpdateReview';
import LikeReview from './LikeReview';
import DeleteReview from './DeleteReview';
// import AdminUpdateReview from '../../AdminPanel/Reviews/AdminUpdateReview';

const AllReviews = ({ id }) => {

  const { token, user } = useContext(AuthContext);
  const classes = useStyles();

  const [reviewsData, setReviewsData] = useState([]);
  // const [votesData, setVotesData] = useState([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [deletedRevId, setDeletedRevId] = useState(0);
  const [getLikes, setLikes] = useState();
  const [getDislikes, setDislikes] = useState();
  const [changeVote, setChangeVote] = useState(false)

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/books/${id}/reviews`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => {
        if (response.status >= 400) {
          throw new Error('Bad Request');
        }
        return response.json()
      })
      .then(data => {

        if (data.length) {
          setReviewsData(data);

        } else {
          return setError(["This book doesn't have reviews."])
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id, deletedRevId, token, changeVote]);

  if (error) {
    return (
      <Grid item xs={12} sm={12} md={12}>
        <Alert severity="error">{error}</Alert>
        {/* <nav>
          <NavLink to="/books">All books</NavLink>
        </nav> */}
      </Grid>
    )
  }

  if (loading) {
    return <Loader />
  }

  const transformedReviews = reviewsData.map(({ bookId, content, dislikes, likes, reviewId, username }) => {

    return (
      <Card disabled key={reviewId} className={classes.card}>
        <div className={classes.cardDetails}>
          <CardContent>
            <Grid>
              <Typography component="h3" variant="body1" color="textSecondary">
                @{username}
              </Typography>
            </Grid>
            <Typography component="h2" variant="h6">
              {content}
            </Typography>

          </CardContent>
          <IconButton color="primary" aria-label="like" onClick={(e) => { e.preventDefault(); LikeReview(1, bookId, reviewId, token, setLikes, setDislikes, setLoading, changeVote, setChangeVote) }}>
            <ThumbUp />
          </IconButton>
          {likes}
          {user.role === 'Admin' || user.username === username ?
            <>
              <IconButton aria-label="delete" onClick={(e) => { e.preventDefault(); DeleteReview(id, reviewId, token, reviewsData, setReviewsData, setDeletedRevId) }}>
                <DeleteIcon />
              </IconButton>
              {/* <IconButton aria-label="edit" onClick={(e) => { e.preventDefault(); AdminUpdateReview(token, bookId, reviewId, reviewsData, setReviewsData)}}>
                <EditOutlined />
              </IconButton> */}
            </>
            : <></>}
          {user.role === 'Admin' || user.username === username ?
            <IconButton aria-label="edit" onClick={(e) => { e.preventDefault(); UpdateReview(bookId, token, reviewId, reviewsData, setReviewsData, content) }}>
              <EditOutlined />
            </IconButton>
            : <></>}
          <IconButton color="secondary" aria-label="dislike" onClick={(e) => { e.preventDefault(); LikeReview(-1, bookId, reviewId, token, setLikes, setDislikes, setLoading, changeVote, setChangeVote) }}>
            <ThumbDown />
          </IconButton>
          {dislikes}
        </div>
      </Card>
    )
  });

  return (
    <>
      {transformedReviews.reverse()}
    </>

  );
}

AllReviews.propTypes = {
  id: PropTypes.number.isRequired,
};

export default AllReviews;
