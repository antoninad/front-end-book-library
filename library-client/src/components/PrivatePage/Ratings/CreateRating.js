import React, {useState, useContext, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
// import Box from '@material-ui/core/Box';
import AuthContext from '../../../providers/AuthContext';
import swal from 'sweetalert';
import BASE_URL from '../../../common/constants';
import Loader from '../../Loader/Loader';
// import useStyles from '../Syles/useStyles';


// eslint-disable-next-line react/prop-types
const CreateRating = ({ id }) => {

  const { token } = useContext(AuthContext);

  const [loading, setLoading] = useState(false);
  const [value, setValue] = React.useState(2);
  const [avg, setAvg] = useState('')

  useEffect(() => {
    fetch(`${BASE_URL}/books/${id}/ratings`,{
      headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
      },
  })
  .then(response => {
    if (response.status >= 400) {
      throw new Error ('Oops... 404 Bad Request');
    };
    return response.json()
  })
  .then(data => {   
      // if(data.message){
      //   return (swal(data.message, "", "error"));
      // }



      setAvg(data.average_rating);
  })
  .catch((err) => swal(err.message, "", "warning"))
  }, [value, id, token])


  const useStyles = makeStyles({
    root: {
      width: 200,
      display: 'flex',
      alignItems: 'center',
    },
  });
  const classes = useStyles();

  const handleCreateRating = (number) => {
    setLoading(true);
    
    fetch(`${BASE_URL}/books/${id}/ratings`,{
      method:'POST',
      body: JSON.stringify({stars: number}),
      headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
      },
  })
  .then(response => response.json())
  .then(data => {   

      if(data.message){
        return (swal("Write a review before rating.", "", "error"));
      }
  })
  .finally(() => setLoading(false));
  };


  if (loading) {
    return <Loader />
  }


  return (
 
    <div className={classes.root}>
      <Rating
        name="hover-feedback"
        value={value}
        precision={1}
        onChange={(event, newValue) => {
          setValue(newValue);
          event.preventDefault()
          handleCreateRating(newValue ? newValue : value)
        }}
      />
      {avg}
      {/* {value !== null && <Box ml={2}>{labels[hover !== -1 ? hover : value]}</Box>} */}
    </div>
      
  
  );
}

export default CreateRating;
