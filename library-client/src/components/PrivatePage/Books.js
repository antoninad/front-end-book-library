import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import { NavLink } from 'react-router-dom';
import swal from 'sweetalert';
import Button from '@material-ui/core/Button';
import useStyles from './Syles/useStyles';
import BookDetails from './BookDetails';
import Loader from '../Loader/Loader';
import Search from './SearchBook/SearchBookByTitle';
import BASE_URL from '../../common/constants';
import AuthContext from '../../providers/AuthContext';
import CreateBook from '../AdminPanel/Books/CreateBook';

const Books = ({ history }) => {
  const { user, token } = useContext(AuthContext);
  const userRole = user.role;

  const classes = useStyles();

  const [bookData, setBookData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error] = useState(null);
  const [search, setSearch] = useState('');
  const [areFiltered, setAreFiltered] = useState(false);
  const [bookAdded,setBookAdded] = useState(0)
 
  const errorAlert = () => { return (swal("The book is not found.", "", "error")) };

  useEffect(() => {
    setLoading(true);
      fetch(`${BASE_URL}/books?title=${search}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then(response => response.json())
        .then(data => {
          setBookData(data)
        })
        .catch((error) => errorAlert())
        .finally(() => setLoading(false));
  }, [search,bookAdded, token] );

  if (error) {
    return (
      <Grid item xs={12} sm={6} md={4}>
        <Alert severity="error">{error}</Alert>
        <nav>
          <NavLink to="/books">All books</NavLink>
        </nav>
      </Grid>
    )
  }

  if (loading) {
    return <Loader />
  }

  const transformedBooks = bookData.map((book) => {
    return (
      <BookDetails key={book.id} {...book} isIndividual={false} />
    )
  })

  const handleChange = (event) => {
    if (event.key === 'Enter') {
      setAreFiltered(true)
      // event.preventDefault(); // Let's stop this event.
      // event.stopPropagation(); // Really this time.
      setSearch(event.target.value)
    }
  }

  const handleAdminHomeButton = () => history.push('/admin/home');

  return (
    <>
      <CssBaseline />
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              All books in our library
            </Typography>
            {userRole === 'Admin' ?
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button variant="contained" color="primary" onClick={(e) => {CreateBook(token, bookAdded, setBookAdded); e.preventDefault()}}>
                    Create a book                
                  </Button>
                </Grid>
                {/* <a>&nbsp;</a><a>&nbsp;</a><a>&nbsp;</a> */}
                <Grid item>
                  <Button variant="contained" color="primary" onClick={handleAdminHomeButton}>
                    Go to Admin Home Page                
                  </Button>
                </Grid>
              </Grid> :
            null}
            <br />
            <Search handleChange={handleChange} />
            {areFiltered ?
              <Button variant="outlined" color="primary" align="right" onClick={() => {setAreFiltered(false); setSearch('');}}> View All Books </Button> : <></>}
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {transformedBooks}
          </Grid>
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        {/* <Typography variant="h6" align="center" gutterBottom>
          Footer
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Something here to give the footer a purpose!
        </Typography> */}

      </footer>
      {/* End footer */}
    </>
  );
}

Books.propTypes = {
  history: PropTypes.object.isRequired,
}
export default Books;
