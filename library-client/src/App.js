import React, { useState } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Home from './components/PublicPage/HomePage/Home';
import Books from './components/PrivatePage/Books';
import IndividualBook from './components/PrivatePage/IndividualBook';
import NotFound from './components/NotFound/NotFound';
import Navbar from './components/PublicPage/Navbar/Navbar';
import Copyright from './components/PublicPage/Copyright/Copyright';
import LogIn from './components/PublicPage/LogIn/LogIn';
import Register from './components/PublicPage/Register/Register';
import AuthContext from './providers/AuthContext';
import GuardedRoute from './providers/GuardedRoute';
import decode from 'jwt-decode';
import AdminHomePage from './components/AdminPanel/AdminHomePage';
import Users from './components/AdminPanel/Users/Users'
import AdminReviews from './components/AdminPanel/Reviews/AdminReviews';
import RevContext from './components/AdminPanel/Reviews/RevContext';

const App = () => {

  const token = localStorage.getItem('token');
  const [authValue, setAuthValue] = useState({
    isLoggedIn: token ? true : false,
    user: token ? decode(token) : null,
    token: token,
  });

  const [fakeRev, setFake] = useState({
    reviewsData: [1, 2, 3],
    update: false,
  })

  return (
    <Router>
      <AuthContext.Provider value={{...authValue, setLoginState: setAuthValue}}>
        <Navbar />
        <Switch>
          <Redirect path="/" exact to="home" />
          <Route path="/home" component={Home} />
          <Route path="/signup" component={Register} />
          <Route path="/signin" component={LogIn} />
          <GuardedRoute path="/books" auth={authValue.isLoggedIn} exact component={Books} />
          <GuardedRoute path="/books/:id" auth={authValue.isLoggedIn} exact component={IndividualBook} />
          <GuardedRoute path="/admin/home" auth={authValue.isLoggedIn && authValue.user.role === 'Admin'} exact component={AdminHomePage} />
          <GuardedRoute path="/admin/users" auth={authValue.isLoggedIn && authValue.user.role === 'Admin'} exact component={Users} />
          <RevContext.Provider value={{...fakeRev, setReviewsState: setFake}}>
            <GuardedRoute path="/admin/reviews" auth={authValue.isLoggedIn && authValue.user.role === 'Admin'} exact component={AdminReviews} />
          </RevContext.Provider>
          <Route path="*" component={NotFound} />
        </Switch>
        <Copyright />
      </AuthContext.Provider>
    </Router>
  );
};

export default App;
